$(document).ready(function() {
    // current symbol's turn
    let currentSymbolTurn = "X";

    // empty tic tac toe board
    let board = [["","",""],
                 ["","",""],
                 ["","",""]];
  // top row
    // toggled when in use
    let topLeftClicked = false;
    let topCenterClicked = false;
    let topRightClicked = false;

    // owner will be either 'X' or 'O'
    let topLeftOwner = "";
    let topCenterOwner = "";
    let topRightOwner = "";

  // middle row
    let middleLeftClicked = false;
    let middleCenterClicked = false;
    let middleRightClicked = false;

    let middleLeftOwner = "";
    let middleCenterOwner = "";
    let middleRightOwner = "";

  // bottom row
    let bottomLeftClicked = false;
    let bottomCenterClicked = false;
    let bottomRightClicked = false;

    let bottomLeftOwner = "";
    let bottomCenterOwner = "";
    let bottomRightOwner = "";

    // check for board cell clicks
    $("span").click(function(event) {

        switch (event.target.id) {

          case "top_left":
            if(!topLeftClicked){
              if(currentSymbolTurn === "X"){
                $(this).css('background-image', 'url("../assets/images/x_bg.png")');
                topLeftOwner = "X";
                currentSymbolTurn = "O";
                board[0][0] = "X";
              }
              else{
                $(this).css('background-image', 'url("../assets/images/o_bg.png")');
                topLeftOwner = "O";
                currentSymbolTurn = "X";
                board[0][0] = "O";
              }
              topLeftClicked = true;
            }
            else{
              // nothing - already set
              //alert("Cell already in use!");
            }
            break;

          case "top_center":
            if(!topCenterClicked){
              if(currentSymbolTurn === "X"){
                $(this).css('background-image', 'url("../assets/images/x_bg.png")');
                topCenterOwner = "X";
                currentSymbolTurn = "O";
                board[0][1] = "X";
              }
              else{
                $(this).css('background-image', 'url("../assets/images/o_bg.png")');
                topCenterOwner = "O";
                currentSymbolTurn = "X";
                board[0][1] = "O";
              }
              topCenterClicked = true;
            }
            else{
              // nothing - already set
              //alert("Cell already in use!");
            }
            break;

          case "top_right":
            if(!topRightClicked){
              if(currentSymbolTurn === "X"){
                $(this).css('background-image', 'url("../assets/images/x_bg.png")');
                topRightOwner = "X";
                currentSymbolTurn = "O";
                board[0][2] = "X";
              }
              else{
                $(this).css('background-image', 'url("../assets/images/o_bg.png")');
                topRightOwner = "O";
                currentSymbolTurn = "X";
                board[0][2] = "O";
              }
              topRightClicked = true;
            }
            else{
              // nothing - already set
              //alert("Cell already in use!");
            }
            break;

          case "middle_left":
            if(!middleLeftClicked){
              if(currentSymbolTurn === "X"){
                $(this).css('background-image', 'url("../assets/images/x_bg.png")');
                middleLeftOwner = "X";
                currentSymbolTurn = "O";
                board[1][0] = "X";
              }
              else{
                $(this).css('background-image', 'url("../assets/images/o_bg.png")');
                middleLeftOwner = "O";
                currentSymbolTurn = "X";
                board[1][0] = "O";
              }
              middleLeftClicked = true;
            }
            else{
              // nothing - already set
              //alert("Cell already in use!");
            }
            break;

          case "middle_center":
            if(!middleCenterClicked){
              if(currentSymbolTurn === "X"){
                $(this).css('background-image', 'url("../assets/images/x_bg.png")');
                middleCenterOwner = "X";
                currentSymbolTurn = "O";
                board[1][1] = "X";
              }
              else{
                $(this).css('background-image', 'url("../assets/images/o_bg.png")');
                middleCenterOwner = "O";
                currentSymbolTurn = "X";
                board[1][1] = "O";
              }
              middleCenterClicked = true;
            }
            else{
              // nothing - already set
              //alert("Cell already in use!");
            }
            break;

          case "middle_right":
            if(!middleRightClicked){
              if(currentSymbolTurn === "X"){
                $(this).css('background-image', 'url("../assets/images/x_bg.png")');
                middleRightOwner = "X";
                currentSymbolTurn = "O";
                board[1][2] = "X";
              }
              else{
                $(this).css('background-image', 'url("../assets/images/o_bg.png")');
                middleRightOwner = "O";
                currentSymbolTurn = "X";
                board[1][2] = "O";
              }
              middleRightClicked = true;
            }
            else{
              // nothing - already set
              //alert("Cell already in use!");
            }
            break;

          case "bottom_left":
            if(!bottomLeftClicked){
              if(currentSymbolTurn === "X"){
                $(this).css('background-image', 'url("../assets/images/x_bg.png")');
                bottomLeftOwner = "X";
                currentSymbolTurn = "O";
                board[2][0] = "X";
              }
              else{
                $(this).css('background-image', 'url("../assets/images/o_bg.png")');
                bottomLeftOwner = "O";
                currentSymbolTurn = "X";
                board[2][0] = "O";
              }
              bottomLeftClicked = true;
            }
            else{
              // nothing - already set
              //alert("Cell already in use!");
            }
            break;

          case "bottom_center":
            if(!bottomCenterClicked){
              if(currentSymbolTurn === "X"){
                $(this).css('background-image', 'url("../assets/images/x_bg.png")');
                bottomCenterOwner = "X";
                currentSymbolTurn = "O";
                board[2][1] = "X";
              }
              else{
                $(this).css('background-image', 'url("../assets/images/o_bg.png")');
                bottomCenterOwner = "O";
                currentSymbolTurn = "X";
                board[2][1] = "O";
              }
              bottomCenterClicked = true;
            }
            else{
              // nothing - already set
              //alert("Cell already in use!");
            }
            break;

          case "bottom_right":
            if(!bottomRightClicked){
              if(currentSymbolTurn === "X"){
                $(this).css('background-image', 'url("../assets/images/x_bg.png")');
                bottomRightOwner = "X";
                currentSymbolTurn = "O";
                board[2][2] = "X";
              }
              else{
                $(this).css('background-image', 'url("../assets/images/o_bg.png")');
                bottomRightOwner = "O";
                currentSymbolTurn = "X";
                board[2][2] = "O";
              }
              bottomRightClicked = true;
            }
            else{
              // nothing - already set
              //alert("Cell already in use!");
            }
            break;

          default:
            break;
        }
        // update UI of current turn
        $("#current_turn").html(currentSymbolTurn);

        // check for tie game occuring
        if(topLeftClicked && topCenterClicked && topRightClicked && middleLeftClicked && middleCenterClicked &&
          middleRightClicked && bottomLeftClicked && bottomCenterClicked && bottomRightClicked){
          $("#game_winner").html("TIE GAME!");
          $("h2").html("");
        }

        //check if someone has won the game yet
        winCheck(board);


    });
});

const winCheck = (board) => {
// checking board cells for any possible win
// in the future we can check only the rows/columns affected by the latest move...

// checking for X wins
  // checking straight accross rows for X win
  if(board[0][0] === "X" && board[0][1] === "X" && board[0][2] === "X"){
    //alert("X WINS!");
    $("#game_winner").html("X WINS!");
    $("h2").html("");
  }
  if(board[1][0] === "X" && board[1][1] === "X" && board[1][2] === "X"){
    //alert("X WINS!");
    $("#game_winner").html("X WINS!");
    $("h2").html("");
  }
  if(board[2][0] === "X" && board[2][1] === "X" && board[2][2] === "X"){
    //alert("X WINS!");
    $("#game_winner").html("X WINS!");
    $("h2").html("");
  }
  // checking diagnals for X win
  if(board[0][0] === "X" && board[1][1] === "X" && board[2][2] === "X"){
    //alert("X WINS!");
    $("#game_winner").html("X WINS!");
    $("h2").html("");
  }
  if(board[0][2] === "X" && board[1][1] === "X" && board[2][0] === "X"){
    //alert("X WINS!");
    $("#game_winner").html("X WINS!");
    $("h2").html("");
  }
  // check columns for X win
  if(board[0][0] === "X" && board[1][0] === "X" && board[2][0] === "X"){
    //alert("X WINS!");
    $("#game_winner").html("X WINS!");
    $("h2").html("");
  }
  if(board[0][1] === "X" && board[1][1] === "X" && board[2][1] === "X"){
    //alert("X WINS!");
    $("#game_winner").html("X WINS!");
    $("h2").html("");
  }
  if(board[0][2] === "X" && board[1][2] === "X" && board[2][2] === "X"){
    //alert("X WINS!");
    $("#game_winner").html("X WINS!");
    $("h2").html("");
  }
// checking for O wins
  // checking straight accross rows for O win
  if(board[0][0] === "O" && board[0][1] === "O" && board[0][2] === "O"){
    //alert("O WINS!");
    $("#game_winner").html("O WINS!");
    $("h2").html("");
  }
  if(board[1][0] === "O" && board[1][1] === "O" && board[1][2] === "O"){
    //alert("O WINS!");
    $("#game_winner").html("O WINS!");
    $("h2").html("");
  }
  if(board[2][0] === "O" && board[2][1] === "O" && board[2][2] === "O"){
    //alert("O WINS!");
    $("#game_winner").html("O WINS!");
    $("h2").html("");
  }// checking diagnals for O win
  if(board[0][0] === "O" && board[1][1] === "O" && board[2][2] === "O"){
    //alert("O WINS!");
    $("#game_winner").html("O WINS!");
    $("h2").html("");
  }
  if(board[0][2] === "O" && board[1][1] === "O" && board[2][0] === "O"){
    //alert("O WINS!");
    $("#game_winner").html("O WINS!");
    $("h2").html("");
  }
  // check columns for O win
  if(board[0][0] === "O" && board[1][0] === "O" && board[2][0] === "O"){
    //alert("O WINS!");
    $("#game_winner").html("O WINS!");
    $("h2").html("");
  }
  if(board[0][1] === "O" && board[1][1] === "O" && board[2][1] === "O"){
    //alert("O WINS!");
    $("#game_winner").html("O WINS!");
    $("h2").html("");
  }
  if(board[0][2] === "O" && board[1][2] === "O" && board[2][2] === "O"){
    //alert("O WINS!");
    $("#game_winner").html("O WINS!");
    $("h2").html("");
  }
}
